package com.example.katayna.battleshipwar;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import java.util.*;
import android.widget.TextView;

public class MyOtherActivity extends AppCompatActivity implements View.OnClickListener {

    private Button[][] buttons = new Button[12][12];
    private Boolean[][] takenButtons = new Boolean[12][12];
    private boolean turn = true;
    int downShips = 0;
    List<Ship> shipList = new ArrayList();
    private int clickCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        AssignButtons();
        CreateTakenButBoard();
        PopulateBattlefield();

        Button resetBtn = (Button) findViewById(R.id.resetBtn);
        resetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(getIntent());
            }
        });

    }

    public void AssignButtons() {
        for(int i = 0; i < 10; i++){
            for(int j = 0; j < 10; j++){
                String buttonID = "button_" + i + j;
                int resID = getResources().getIdentifier(buttonID, "id", getPackageName());
                buttons[i+1][j+1] = findViewById(resID);
                buttons[i+1][j+1].setOnClickListener(this);
            }
        }
    }

//Imaginary table, all free
    public void CreateTakenButBoard() {
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 12; j++) {
                takenButtons[i][j] = false;
            }
        }
    }


    public void PopulateBattlefield() {
        for (int i = 4; i > 1; i--) {
            CreateShip(i, 6-i);
        }
    }

    public void CreateShip(int type, int amount) {

        Boolean addShip = false;
        for (int x = 0; x < amount; x++) {
            Random r = new Random();
            int vh = r.nextInt(2);
            if (vh == 0){ // if = we do horizontal

                Random i = new Random();
                Random j = new Random();
                int s = i.nextInt(10+1-1)+1;
                int z = j.nextInt(12-type+1-2)+2;

                if (z!=0 && s!=0){

                    if (shipList.size() != 0) {
                        if (type == 4) {
                            if (takenButtons[s][z] == true || takenButtons[s][z - 1] == true || takenButtons[s][z + 1] == true || takenButtons[s][z + 2] == true) {
                                addShip = false;
                                x--;
                            } else {
                                addShip = true;
                            }
                        }
                        if (type == 3) {
                            if (takenButtons[s][z] == true || takenButtons[s][z-1] == true || takenButtons[s][z+1] == true) {
                                addShip = false;
                                x--;
                            } else {
                                addShip = true;
                            }
                        }
                        if (type == 2) {
                            if (takenButtons[s][z] == true || takenButtons[s][z-1] == true) {
                                addShip = false;
                                x--;
                            } else {
                                addShip = true;
                            }
                        }
                    } else {
                        addShip = true;
                    }
                    if (addShip.equals(true)) {
                        Ship ship = new Ship(type);

                        for (int g = 0; g < type; g++) {
                            ship.insertShipButton(g, buttons[s][z - 1 + g]);
                        }
                        for (int g = -1 ; g < 2 ; g++) {
                            for (int k = -2; k < type; k++) {
                                takenButtons[s+g][z+k] = true;
                            }
                        }

                        shipList.add(ship);
                        addShip = false;
                    }


                }
                else{
                    x--;
                }
            }
            if (vh == 1) { // we do vertical
                Random i = new Random();
                Random j = new Random();
                int s = i.nextInt(12-type+1-2)+2;
                int z = j.nextInt(10+1-1)+1;

                if (z!=0 && s!=0){
                    // if we making amount 3  1 2

                    if (shipList.size() != 0) {
                        if (type == 4) {
                            if (takenButtons[s][z] == true || takenButtons[s - 1][z] == true || takenButtons[s + 1][z] == true || takenButtons[s + 2][z] == true) {
                                addShip = false;
                                x--;
                            } else {
                                addShip = true;
                            }
                        }
                        if (type == 3) {
                            if (takenButtons[s][z] == true || takenButtons[s-1][z] == true || takenButtons[s+1][z] == true) {
                                addShip = false;
                                x--;
                            } else {
                                addShip = true;
                            }
                        }
                        if (type == 2) {
                            if (takenButtons[s][z] == true || takenButtons[s-1][z] == true) {
                                addShip = false;
                                x--;
                            } else {
                                addShip = true;
                            }
                        }
                    }
                    else {
                        addShip = true;
                    }
                    if (addShip.equals(true)) {
                        Ship ship = new Ship(type);

                        for (int g = 0; g < type; g++) {
                            ship.insertShipButton(g, buttons[s-1+g][z]);
                        }

                        for (int g = -1 ; g < 2 ; g++) {
                            for (int k = -2; k < type; k++) {
                                takenButtons[s+k][z+g] = true;
                            }
                        }

                        shipList.add(ship);
                        addShip = false;
                    }


                }
                else{
                    x--;
                }
            }

        }
    }



    @Override
    public void onClick(View v) {
        //if this button is not equal to empty string
        if(!((Button) v).getText().toString().equals("")){
            return;
        }
        if(turn){
            TextView result = (TextView) findViewById(R.id.resultTextView);
            ((Button) v).setText(".");
            //String info = getResources().getResourceEntryName(v.getId());
           // result.setText(info);


            for (int i = 0; i < shipList.size(); i++) {

                for (int j = 0; j < shipList.get(i).getShipSize(); j++) {
                    if (shipList.get(i).getShipButton(j).getId() == v.getId()){
                        ((Button) v).setBackgroundColor(Color.GREEN);
                        ((Button) v).setText("X");

                        if (shipList.get(i).shipDown() == true) {

                            downShips++;
                            if(downShips == 9){
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if(!isFinishing()){
                                            new AlertDialog.Builder(MyOtherActivity.this)
                                                    .setTitle("****")
                                                    .setMessage("You killed all ships, Congratulations!!")
                                                    .setCancelable(false)
                                                    .setPositiveButton("START A NEW GAME", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            finish();
                                                            startActivity(getIntent());
                                                        }
                                                    }).show();
                                        }
                                    }
                                });
                            }

                            result.setText("You killed a ship of size "+ shipList.get(i).getShipSize());
                            shipList.get(i).getShipButton(j).setBackgroundColor(Color.RED);


                            /*
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if(!isFinishing()){
                                        new AlertDialog.Builder(MyOtherActivity.this)
                                                .setTitle("Congrats")
                                                .setMessage("You killed a ship")
                                                .setCancelable(false)
                                                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {

                                                    }
                                                }).show();
                                    }
                                }
                            });
                            */
                        }

                    }

                }
            }
        }
        clickCount++;
    }



}


